const Hapi = require('hapi');
const Joi = require('joi');

const config = require('./config');

const server = new Hapi.Server();

server.connection({ port: 3002, host: 'localhost' });


server.register([require('hapi-redis'), require('./lib/plugins/redis'), require('./lib/plugins/active-users-plugin')], (err) => {
  if (err) {
    throw err;
  }

  server.route({
    method: 'POST',
    path: '/register-user',
    config: {
      pre: [
        {
          method: (request, reply) => {
            const { id, username, phoneNumber } = request.payload;

            request.server.plugins['active-users'].service
              .registerUser({ id, username, phoneNumber })
              .then(reply);
          },
          assign: 'users',
        }
      ],
      handler: function (request, reply) {
        reply(request.pre.users);
      },
      validate: {
        payload: {
          id: Joi.string().required(),
          username: Joi.string().required(),
          phoneNumber: Joi.string().length(11).required(),
        }
      }
    }
  });

  server.route({
    method: 'POST',
    path: '/deregister-user',
    config: {
      pre: [
        {
          method: (request, reply) => {
            const { id } = request.payload;

            request.server.plugins['active-users'].service
              .deregisterUser({ id })
              .then(reply);
          },
          assign: 'users',
        }
      ],
      handler: function (request, reply) {
        reply(request.pre.users);
      },
      validate: {
        payload: {
          id: Joi.string().required(),
        }
      }
    }
  });

  server.start((err) => {
    if (err) {
      throw err;
    }
    console.log(`Server running at: ${server.info.uri}`);
  });
});
