const ActiveUsersService = require('../service/active-users-service');

const ActiveUsersPlugin = {
  register: function (server, options, next) {
    const redis = server.plugins['redis'].client;
    const activeUsersService = ActiveUsersService(redis);

    server.expose('service', activeUsersService);

    next();
  },
};

ActiveUsersPlugin.register.attributes = {
  name: 'active-users',
  version: '1.0.0',
  dependencies: 'redis',
};

module.exports = ActiveUsersPlugin;