const Promise = require('bluebird');

const RedisPlugin = {
  register: function (server, options, next) {
    const redisClient = server.plugins['hapi-redis'].client;
    server.expose('client', Promise.promisifyAll(redisClient));
    next();
  },
};

RedisPlugin.register.attributes = {
  name: 'redis',
  version: '1.0.0',
  dependencies: 'hapi-redis',
};

module.exports = RedisPlugin;