const Promise = require('bluebird');

const ActiveUsersService = (redisClient) => {
  const userCollectionKey = 'mos-regsitered-users';

  getRegisteredUsers = () => {
    return redisClient
      .getAsync(userCollectionKey)
      .then(res => res === null ? {} : JSON.parse(res));
  };

  registerUser = ({ id, username, phoneNumber }) => {
    return new Promise((resolve, reject) => {
      getRegisteredUsers()
        .then(users => {
          users[id] = { id, username, phoneNumber, };
          return users;
        })
        .then(users => redisClient.setAsync(userCollectionKey, JSON.stringify(users)))
        .then(getRegisteredUsers)
        .then(resolve)
        .catch(reject);
    });
  };

  deregisterUser = ({ id }) => {
    return new Promise((resolve, reject) => {
      getRegisteredUsers()
        .then(users => {
          delete users[id];
          return redisClient.setAsync(userCollectionKey, JSON.stringify(users));
        })
        .then(getRegisteredUsers)
        .then(resolve)
        .catch(reject);
    });
  };

  return {
    getRegisteredUsers,
    registerUser,
    deregisterUser,
  };
};

module.exports = ActiveUsersService;